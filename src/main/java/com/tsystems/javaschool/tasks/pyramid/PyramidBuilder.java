package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int lines = Double.valueOf(Math.sqrt(inputNumbers.size() * 2)).intValue();

        if (lines * (lines + 1) / 2 != inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }

        inputNumbers.sort(Integer::compareTo);

        int lineLen = lines * 2 - 1;
        int[][] answer = new int [lines][lineLen];
        int idx = 0;

        for (int i = 0; i < lines; i++) {
            int startIdx = (lineLen - ((i + 1) * 2 - 1)) / 2;

            for (int j = startIdx; j < lineLen - startIdx; j += 2) {
                answer[i][j] = inputNumbers.get(idx);
                idx++;
            }

        }

        return answer;
    }


}
