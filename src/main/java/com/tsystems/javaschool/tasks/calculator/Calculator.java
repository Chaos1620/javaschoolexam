package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class Calculator {

    private final String OPERATORS = "()+-*/";


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }

        statement = statement.trim();

        if (statement.isEmpty()) {
            return null;
        }

        statement = handlingMinus(statement);

        try {
            List<String> reversePolishNotation = toReversePolishNotation(statement);
            return evaluate(reversePolishNotation);
        } catch (NoSuchElementException ex) {
            return null;
        }
    }

    private String handlingMinus(String statement) {
        if (statement.charAt(0) == '-') {
            statement = '0' + statement;
        }
        for (int i = statement.indexOf("-"); i != -1; i = statement.indexOf("-", i + 1)) {
            if (statement.charAt(i - 1) == '(') {
                statement = statement.substring(0, i) + '0' + statement.substring(i);
            }
        }

        return statement;
    }

    private List<String> toReversePolishNotation(String statement) {
        LinkedList<Character> stack = new LinkedList<>();
        List<String> result = new ArrayList<>();
        StringBuilder number = new StringBuilder();

        for (int i = 0; i < statement.length(); i++) {
            char current = statement.charAt(i);

            if (OPERATORS.indexOf(current) >= 0) {
                if (!number.toString().isEmpty()) {
                    result.add(number.toString());
                    number = new StringBuilder();
                }

                sortOperator(current, stack, result);
            } else {
                number.append(current);
            }

        }

        if (!number.toString().isEmpty()) {
            result.add(number.toString());
        }

        while (!stack.isEmpty()) {
            result.add(String.valueOf(stack.removeLast()));
        }

        return result;
    }

    private void sortOperator(char operator, LinkedList<Character> stack, List<String> result) {
        if (stack.isEmpty()) {
            stack.addLast(operator);
        }
        else {
            switch (operator) {
                case '(':
                    stack.addLast(operator);
                    break;
                case ')':
                    while (stack.getLast() != '('){
                        result.add(String.valueOf(stack.removeLast()));
                    }
                    stack.removeLast();
                    break;
                default:
                    if (OPERATORS.indexOf(operator) <= OPERATORS.indexOf(stack.getLast())) {
                        result.add(String.valueOf(stack.removeLast()));
                    }
                    stack.addLast(operator);
                    break;
            }
        }
    }

    private String evaluate(List<String> statement) {
        LinkedList<Double> answer = new LinkedList<>();

        for (String it : statement) {
            if (OPERATORS.contains(it)) {
                if (answer.size() < 2) {
                    return null;
                }
                else {
                    Double num2 = answer.removeLast();
                    Double num1 = answer.removeLast();

                    switch (it) {
                        case "-":
                            answer.addLast(num1 - num2);
                            break;
                        case "+":
                            answer.addLast(num1 + num2);
                            break;
                        case "/":
                            if (num2 == 0) {
                                return null;
                            }
                            answer.addLast(num1 / num2);
                            break;
                        case "*":
                            answer.addLast(num1 * num2);
                            break;
                        default:
                            return null;
                    }
                }
            }
            else {
                try {
                    answer.addLast(Double.valueOf(it));
                } catch (NumberFormatException ex) {
                    return null;
                }
            }
        }

        if (answer.size() != 1) {
            return null;
        }

        BigDecimal result = BigDecimal.valueOf(answer.getLast());
        return result.longValue() == result.doubleValue() ? String.valueOf(result.longValue())
                : String.valueOf(result.setScale(4, RoundingMode.HALF_UP).doubleValue());
    }

}
